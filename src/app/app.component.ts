import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TrackingService } from './tracking.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  track : FormGroup
  constructor(private formBuilder : FormBuilder, private trackingService: TrackingService){}
  ngOnInit(): void {
    this.track=this.formBuilder.group({
      tracking:['']
    })
  }
  trackingData:any[]=[];
  orderData:any;

  onSubmit(track:any){
  debugger;
  var Id= track.value.tracking;
    this.trackingService.postId(Id).subscribe((result:any)=>{
      //this.trackingData=result;
      debugger;
      this.trackingData = result.data.awbTrackingDetails;
      this.orderData = result.data.orderDetail;
      // alert('success /n/n'+JSON.stringify(result));
      alert('success');
    },error=>{
      debugger;
    console.log(error);

    }
    
    )


  }
  title = 'tracking';
}
